class levelList {
    constructor() {
        this.groupMain = { groups : [
                {
                    id: 1,
                    name: "Дети",
                    groups: [
                        {
                            id: 2,
                            name: "Имеющие редкие заболевания",
                            groups: [
                                {
                                    id: 3,
                                    name: "Spina Bifida",
                                    groups: []
                                },
                                {
                                    id: 4,
                                    name: "Буллёзный эпидермолиз",
                                    groups: []
                                }
                            ]
                        },
                        {
                            id: 5,
                            name: "С инвалидностью",
                            groups: []
                        }
                    ]
                },
                {
                    id: 6,
                    name: "Профессиональные сообщества",
                    groups: []
                },
            ] };
        this.listWrapper = document.querySelector('.js-level-list-wrapper');

        !!this.listWrapper && this.createLevel(this.groupMain, this.listWrapper);
    }

    createCheckbox(group) {
        const label = document.createElement('label');
        const labelText = document.createElement('span')
        labelText.innerText = group.name

        const input = document.createElement('input');
        input.className = 'js-dropdown-close';
        input.id = group.id;
        input.type="checkbox";


        label.appendChild(input);
        label.appendChild(labelText);

        return label;
    }

    createLevel(group, element) {
        if (group.groups.length > 0) {
            const list = document.createElement('ul');
            list.className = 'level-list js-level-list'

            const hasPrevGroups = !(element.classList.contains('level-list-wrapper'))

            if (hasPrevGroups) {
                const groupElement = document.createElement('li')
                groupElement.className = 'level-list__back js-level-back'
                groupElement.innerText = group.name

                list.appendChild(groupElement);
            }

            group.groups.forEach(groupChild => {
                const groupElement = document.createElement('li');

                if (groupChild.groups.length > 0) {
                    groupElement.className = 'level-list__next js-level-next'
                    groupElement.innerText = groupChild.name
                } else {
                    groupElement.className = 'level-list__select'
                    const checkbox = this.createCheckbox(groupChild)
                    groupElement.appendChild(checkbox);
                }

                list.appendChild(groupElement);
                element.appendChild(list);

                this.createLevel(groupChild, groupElement);
            });
        }
    }
}

new levelList();