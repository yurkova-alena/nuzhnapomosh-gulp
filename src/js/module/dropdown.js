class Dropdown {
    constructor() {
        this.wrapper = document.querySelector('.js-dropdown-wrapper');

        !!this.wrapper && this.bindHandlers();
    }

    init() {
        const wrapper = this.wrapper;
        const trigger = this.wrapper.querySelector('.js-dropdown-trigger');
        const triggersClose = document.querySelectorAll('.js-dropdown-close');
        const dropdown = this.wrapper.querySelector('.js-dropdown');

        [].map.call(triggersClose, (trigger) => {
            trigger.addEventListener('change', () => {
                trigger.checked && dropdown.classList.remove('active');
            })
        });

        trigger.addEventListener('click', () => {
            dropdown.classList.toggle('active');
        })

        document.body.addEventListener('click', function (event) {
            let target = event.composedPath().includes(wrapper);
            !target && dropdown.classList.remove('active');
        })
    }

    bindHandlers() {
        this.init()
    }
}

new Dropdown();