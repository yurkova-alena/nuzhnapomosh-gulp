class levelProcess {
    constructor() {
        this.listWrapper = document.querySelector('.level-list-wrapper');
        this.activeClass = 'active';

        !!this.listWrapper && this.bindHandlers();
    }

    bindHandlers() {
        this.triggersBack = this.listWrapper.querySelectorAll('.js-level-back');
        this.triggersNext = this.listWrapper.querySelectorAll('.js-level-next');

        [].map.call(this.triggersNext, (trigger) => {
            trigger.addEventListener('click', e => {
                e.stopPropagation();
                trigger.querySelector('.level-list').classList.add(this.activeClass);
            })
        });

        [].map.call(this.triggersBack, (trigger) => {
            trigger.addEventListener('click', e => {
                e.stopPropagation();
                trigger.closest('.level-list').classList.remove(this.activeClass);
            })
        });
    }
}

new levelProcess();