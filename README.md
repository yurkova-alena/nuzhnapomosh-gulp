# Neo start template (updated by @Alyona)

### Command line options
    'npm install' - install all dependencies;
    'npm start' – for start working;
    'npm run build' - build dev version of project from sources;

### Structure
`/src/` - thats where you write code.

`/dist/` - compiled code. Do not ever edit this folder.

### Naming
We use BEM naming, meaning `.block` for independent block. `.block__element` for elements inside that block, `.block_modification` for modification of the block or element. Also we allow bem-mixins.


For javascript hooks we use prefix `.js-*`.

You are welcome

### Here we use following libraries:

* [Swiper](https://swiperjs.com/swiper-api)
* [Micromodal](https://micromodal.now.sh/)
* [ISpin](https://unmanner.github.io/ispinjs/)
* [body-scroll-lock](https://github.com/willmcpo/body-scroll-lock)
* [noUiSlider](https://refreshless.com/nouislider/)
* [dom-slider](https://github.com/BrentonCozby/dom-slider)