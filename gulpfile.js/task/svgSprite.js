const svgSprite = () => {
    return $.gulp.src($.path.svgSprite.src)
        .pipe($.gp.plumber({
            errorHandler: $.gp.notify.onError(error => ({
                title: 'Svg',
                message: error.message
            }))
        }))
        .pipe($.gp.svgSprite($.app.svgSprite))
        .pipe($.gulp.dest($.path.svgSprite.dest));
}

module.exports = svgSprite;