global.$ = {
    // Пакеты
    gulp: require('gulp'),
    gp: require('gulp-load-plugins')(),
    browserSync: require('browser-sync').create(),

    // Конфигурация
    path: require('./config/path'),
    app: require('./config/app')
}

// Задачи
const requireDir = require('require-dir');
const task = requireDir('./task', {recurse: true});

// Наблюдение
const watcher = () => {
    $.gulp.watch($.path.pug.watch, task.pug).on('all', $.browserSync.reload);
    $.gulp.watch($.path.scss.watch, task.scss).on('all', $.browserSync.reload);
    $.gulp.watch($.path.js.watch, task.js).on('all', $.browserSync.reload);
    $.gulp.watch($.path.img.watch, task.img).on('all', $.browserSync.reload);
    $.gulp.watch($.path.font.watch, task.font).on('all', $.browserSync.reload);
    $.gulp.watch($.path.svgSprite.watch, task.svgSprite).on('all', $.browserSync.reload);
}

const build = $.gulp.series(
    task.clear,
    $.gulp.parallel(task.pug, task.scss, task.js, task.img, task.font, task.svgSprite)
);

const dev = $.gulp.series(
    build,
    $.gulp.parallel(watcher, task.server)
);

//Задачи
exports.pug = task.pug;
exports.scss = task.scss;
exports.js = task.js;
exports.img = task.img;
exports.font = task.font;
exports.svgSprite = task.svgSprite;

// Сборка
exports.default = $.app.isProd
    ? build
    : dev;