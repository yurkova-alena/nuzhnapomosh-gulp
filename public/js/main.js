/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _module_level_list__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./module/level-list */ \"./src/js/module/level-list.js\");\n/* harmony import */ var _module_level_list__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_module_level_list__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _module_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./module/dropdown */ \"./src/js/module/dropdown.js\");\n/* harmony import */ var _module_dropdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_module_dropdown__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _module_level_process__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./module/level-process */ \"./src/js/module/level-process.js\");\n/* harmony import */ var _module_level_process__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_module_level_process__WEBPACK_IMPORTED_MODULE_2__);\n\r\n\r\n\n\n//# sourceURL=webpack://gulp-start/./src/js/main.js?");

/***/ }),

/***/ "./src/js/module/dropdown.js":
/*!***********************************!*\
  !*** ./src/js/module/dropdown.js ***!
  \***********************************/
/***/ (function() {

eval("class Dropdown {\r\n    constructor() {\r\n        this.wrapper = document.querySelector('.js-dropdown-wrapper');\r\n\r\n        !!this.wrapper && this.bindHandlers();\r\n    }\r\n\r\n    init() {\r\n        const wrapper = this.wrapper;\r\n        const trigger = this.wrapper.querySelector('.js-dropdown-trigger');\r\n        const triggersClose = document.querySelectorAll('.js-dropdown-close');\r\n        const dropdown = this.wrapper.querySelector('.js-dropdown');\r\n\r\n        [].map.call(triggersClose, (trigger) => {\r\n            trigger.addEventListener('change', () => {\r\n                trigger.checked && dropdown.classList.remove('active');\r\n            })\r\n        });\r\n\r\n        trigger.addEventListener('click', () => {\r\n            dropdown.classList.toggle('active');\r\n        })\r\n\r\n        document.body.addEventListener('click', function (event) {\r\n            let target = event.composedPath().includes(wrapper);\r\n            !target && dropdown.classList.remove('active');\r\n        })\r\n    }\r\n\r\n    bindHandlers() {\r\n        this.init()\r\n    }\r\n}\r\n\r\nnew Dropdown();\n\n//# sourceURL=webpack://gulp-start/./src/js/module/dropdown.js?");

/***/ }),

/***/ "./src/js/module/level-list.js":
/*!*************************************!*\
  !*** ./src/js/module/level-list.js ***!
  \*************************************/
/***/ (function() {

eval("class levelList {\r\n    constructor() {\r\n        this.groupMain = { groups : [\r\n                {\r\n                    id: 1,\r\n                    name: \"Дети\",\r\n                    groups: [\r\n                        {\r\n                            id: 2,\r\n                            name: \"Имеющие редкие заболевания\",\r\n                            groups: [\r\n                                {\r\n                                    id: 3,\r\n                                    name: \"Spina Bifida\",\r\n                                    groups: []\r\n                                },\r\n                                {\r\n                                    id: 4,\r\n                                    name: \"Буллёзный эпидермолиз\",\r\n                                    groups: []\r\n                                }\r\n                            ]\r\n                        },\r\n                        {\r\n                            id: 5,\r\n                            name: \"С инвалидностью\",\r\n                            groups: []\r\n                        }\r\n                    ]\r\n                },\r\n                {\r\n                    id: 6,\r\n                    name: \"Профессиональные сообщества\",\r\n                    groups: []\r\n                },\r\n            ] };\r\n        this.listWrapper = document.querySelector('.js-level-list-wrapper');\r\n\r\n        !!this.listWrapper && this.createLevel(this.groupMain, this.listWrapper);\r\n    }\r\n\r\n    createCheckbox(group) {\r\n        const label = document.createElement('label');\r\n        const labelText = document.createElement('span')\r\n        labelText.innerText = group.name\r\n\r\n        const input = document.createElement('input');\r\n        input.className = 'js-dropdown-close';\r\n        input.id = group.id;\r\n        input.type=\"checkbox\";\r\n\r\n\r\n        label.appendChild(input);\r\n        label.appendChild(labelText);\r\n\r\n        return label;\r\n    }\r\n\r\n    createLevel(group, element) {\r\n        if (group.groups.length > 0) {\r\n            const list = document.createElement('ul');\r\n            list.className = 'level-list js-level-list'\r\n\r\n            const hasPrevGroups = !(element.classList.contains('level-list-wrapper'))\r\n\r\n            if (hasPrevGroups) {\r\n                const groupElement = document.createElement('li')\r\n                groupElement.className = 'level-list__back js-level-back'\r\n                groupElement.innerText = group.name\r\n\r\n                list.appendChild(groupElement);\r\n            }\r\n\r\n            group.groups.forEach(groupChild => {\r\n                const groupElement = document.createElement('li');\r\n\r\n                if (groupChild.groups.length > 0) {\r\n                    groupElement.className = 'level-list__next js-level-next'\r\n                    groupElement.innerText = groupChild.name\r\n                } else {\r\n                    groupElement.className = 'level-list__select'\r\n                    const checkbox = this.createCheckbox(groupChild)\r\n                    groupElement.appendChild(checkbox);\r\n                }\r\n\r\n                list.appendChild(groupElement);\r\n                element.appendChild(list);\r\n\r\n                this.createLevel(groupChild, groupElement);\r\n            });\r\n        }\r\n    }\r\n}\r\n\r\nnew levelList();\n\n//# sourceURL=webpack://gulp-start/./src/js/module/level-list.js?");

/***/ }),

/***/ "./src/js/module/level-process.js":
/*!****************************************!*\
  !*** ./src/js/module/level-process.js ***!
  \****************************************/
/***/ (function() {

eval("class levelProcess {\r\n    constructor() {\r\n        this.listWrapper = document.querySelector('.level-list-wrapper');\r\n        this.activeClass = 'active';\r\n\r\n        !!this.listWrapper && this.bindHandlers();\r\n    }\r\n\r\n    bindHandlers() {\r\n        this.triggersBack = this.listWrapper.querySelectorAll('.js-level-back');\r\n        this.triggersNext = this.listWrapper.querySelectorAll('.js-level-next');\r\n\r\n        [].map.call(this.triggersNext, (trigger) => {\r\n            trigger.addEventListener('click', e => {\r\n                e.stopPropagation();\r\n                trigger.querySelector('.level-list').classList.add(this.activeClass);\r\n            })\r\n        });\r\n\r\n        [].map.call(this.triggersBack, (trigger) => {\r\n            trigger.addEventListener('click', e => {\r\n                e.stopPropagation();\r\n                trigger.closest('.level-list').classList.remove(this.activeClass);\r\n            })\r\n        });\r\n    }\r\n}\r\n\r\nnew levelProcess();\n\n//# sourceURL=webpack://gulp-start/./src/js/module/level-process.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/main.js");
/******/ 	
/******/ })()
;